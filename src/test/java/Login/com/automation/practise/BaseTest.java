package Login.com.automation.practise;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import io.github.bonigarcia.wdm.ChromeDriverManager;

public abstract class BaseTest {
	protected WebDriver driver;
	
	@BeforeMethod
	public void OpenBrowser () {
		
		ChromeDriverManager.getInstance().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	} 
	
	@AfterMethod
	public void CloseBrowser () {
		if(null != driver) {
			driver.quit();
		}
	}


}
