package Login.com.automation.practise;

import static org.testng.Assert.assertEquals;


import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.automation.dto.User.*;
import com.automation.pages.AccountPage;
import com.automation.pages.RegisterPage;
import com.automation.pages.SinginPage;

public class SingInTest extends BaseTest {
	private DtoUser User1;
	private String mail;
	private String password; 

	@BeforeSuite
	 
	public void SetUpData () {
		mail = RandomStringUtils.randomAlphabetic(5)+ "@testautomation.com";
		password = RandomStringUtils.randomNumeric(6);
		User1 = new DtoUser ("Female", "Celeste","Azul",password,"25", "4","1992","Info", "Libertad 5215", "abcd","5","12345", "21","154811199", "Azuleta");
	}
	
	@Test
	public void Singintest() {
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		
		SinginPage singInpage = new SinginPage(driver);
		singInpage.SingIn("cdd@test.com", "123456789");
		singInpage.submitLogin();
		AccountPage accountcostumer = new AccountPage(driver);
		accountcostumer.getCustomerAccount();
		assertEquals(accountcostumer.getCustomerAccount(), "Mar�a Alegre");
			
	}
	
	@Test
	public void RegisterFormtest () {
		
		driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
		
		SinginPage signInpage = new SinginPage(driver);
		RegisterPage registerpage = signInpage.CreateAccount(mail);
		registerpage.Registrationform(User1);
		registerpage.clickRegister();
		AccountPage Newconsumer = new AccountPage (driver);
		Newconsumer.getCustomerAccount();
		assertEquals ("Welcome to your account. Here you can manage all of your personal information and orders.", Newconsumer.getMessage());
			
	}
	
}
