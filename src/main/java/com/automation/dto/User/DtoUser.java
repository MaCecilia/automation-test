package com.automation.dto.User;

	
	public class DtoUser {
		
		private String genero;
		private String nombre;
		private String apellido;
		private String contrasena;
		private String dianac;
		private String mesnac;
		private String anonac;
		private String empresa;
		private String direccion;
		private String direccion2;
		private String ciudad;
		private String estado;
		private String codpostal; 
		private String pais;
		private String descripcion;
		private String phone;
		private String mobile;
		private String alias;
		
		
		
		public String getGenero() {
			return genero;
		}
		public void setGenero(String genero) {
			this.genero = genero;
		}
		
		public String getNombre() {
			return nombre;
		}
		public String getApellido() {
			return apellido;
		}
		
		public String getContrasena() {
			return contrasena;
		}
		public String getDianac() {
			return dianac;
		}
		public String getMesnac() {
			return mesnac;
		}
		public String getAnonac() {
			return anonac;
		}
		public String getEmpresa() {
			return empresa;
		}
		public String getDireccion() {
			return direccion;
		}
		public String getDireccion2() {
			return direccion2;
		}
		public String getCiudad() {
			return ciudad;
		}
		public String getEstado() {
			return estado;
		}
		public String getCodpostal() {
			return codpostal;
		}
		public String getPais() {
			return pais;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public String getPhone() {
			return phone;
		}
		public String getMobile() {
			return mobile;
		}
		public String getAlias() {
			return alias;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public void setApellido(String apellido) {
			this.apellido = apellido;
		}

		public void setContrasena(String contrasena) {
			this.contrasena = contrasena;
		}
		public void setDianac(String dianac) {
			this.dianac = dianac;
		}
		public void setMesnac(String mesnac) {
			this.mesnac = mesnac;
		}
		public void setAnonac(String anonac) {
			this.anonac = anonac;
		}
		public void setEmpresa(String empresa) {
			this.empresa = empresa;
		}
		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}
		public void setDireccion2(String direccion2) {
			this.direccion2 = direccion2;
		}
		public void setCiudad(String ciudad) {
			this.ciudad = ciudad;
		}
		public void setEstado(String estado) {
			this.estado = estado;
		}
		public void setCodpostal(String codpostal) {
			this.codpostal = codpostal;
		}
		public void setPais(String pais) {
			this.pais = pais;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public void setMobile(String mobile) {
			this.mobile = mobile;
		}
		public void setAlias(String alias) {
			this.alias = alias;
		}
		public DtoUser(String genero, String nombre, String apellido, String contrasena, String dianac, String mesnac,
				String anonac, String empresa, String direccion, String ciudad, String estado, String codpostal, String pais,
				String mobile, String alias) {
			super();
			this.genero = genero;
			this.nombre = nombre;
			this.apellido = apellido;
			this.contrasena = contrasena;
			this.dianac = dianac;
			this.mesnac = mesnac;
			this.anonac = anonac;
			this.empresa = empresa;
			this.direccion = direccion;
			this.ciudad = ciudad;
			this.estado = estado;
			this.codpostal = codpostal;
			this.pais = pais;
			this.mobile = mobile;
			this.alias = alias;
		}
	
	}

