package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SinginPage extends BasePage {
	
	public SinginPage(WebDriver driver) {
		super(driver);
	}

	@FindBy (xpath = "//input[@id='email']")
	private WebElement SingInemail;
	
	@FindBy (xpath = "//input[@id='passwd']" )
	private WebElement SingInPass;
	
	@FindBy (xpath ="//button[@id='SubmitLogin']")
	private WebElement SingInBtn;
	
	@FindBy (xpath = "//input[@name ='email_create']")
	private WebElement EmailCreate;
	
	@FindBy (xpath = "//button[@id = 'SubmitCreate']")
	private WebElement SubmitBTN;
		
	public void SingIn(String mail, String password) {
		SingInemail.clear();
		SingInemail.sendKeys(mail);
		
		SingInPass.clear();
		SingInPass.sendKeys(password);
	}
	
	public RegisterPage CreateAccount (String mail) {
		EmailCreate.clear();
		EmailCreate.sendKeys(mail);
		
		SubmitBTN.click();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		return new RegisterPage(driver);
	}
	
	
	public AccountPage submitLogin() {
		SingInBtn.click();
		return new AccountPage(driver);
	}

}
