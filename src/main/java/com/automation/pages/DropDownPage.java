package com.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class DropDownPage extends BasePage {

	public DropDownPage(WebDriver driver) {
		super(driver);
	}
	@FindBy (xpath = "//select[@id='id_state']" )
	private WebElement state;
	
	@FindBy (xpath = "//select[@id='id_country']")
	private WebElement country;
	
	@FindBy (xpath ="//select[@id = 'days']") 
	private WebElement Days;
	
	@FindBy (xpath = "//select [@id = 'months']")
	private WebElement Months;
	
	@FindBy (xpath = "//select [@id = 'years']")
	private WebElement Years;
	
	public void selectOptionByValueDays (String value) {
		Select dropdownSelect = new Select(Days);
		dropdownSelect.selectByValue(value);;
	}
	
	public void selectOptionByValueMonths (String value) {
		Select dropdownSelect = new Select(Months);
		dropdownSelect.selectByValue(value);
	}
	
	public void selectOptionByValueYears (String value) {
		Select dropdownSelect = new Select (Years);
		dropdownSelect.selectByValue(value);
	}
	
	public void selectOptionByValueState (String value) {
		Select dropdownSelect = new Select (state);
		dropdownSelect.selectByValue(value);
	}
	
	public void selectOptionByValueCountry (String value) {
		Select dropdownSelect = new Select (country);
		dropdownSelect.selectByValue(value);
		
	}
}
