package com.automation.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AccountPage extends BasePage {

	public AccountPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy (xpath = "//a[@class='account']/span")
	private WebElement Customeraccount;
	
	@FindBy (xpath = "//p[@class='info-account']")
	private WebElement Message;
	
	public String getMessage() {
		driver.findElement(By.xpath("//p[@class='info-account']"));
		return Message.getText();
	}
	
	public String getCustomerAccount() {
		driver.findElement(By.xpath("//a[@class='account']/span"));
		return Customeraccount.getText();
	}

}
