package com.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.automation.dto.User.*; 

public class RegisterPage extends BasePage{

	public RegisterPage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy (xpath = "//input[@id='customer_firstname']" )
	private WebElement firstname;
	
	@FindBy (xpath = "//input[@id='customer_lastname']" )
	private WebElement lastname;
	
	@FindBy (xpath = "//input[@id='passwd']")
	private WebElement password;
	
	@FindBy (xpath = "//input[@id='company']" )
	private WebElement company;
	
	@FindBy (xpath = "//input[@id='address1']" )
	private WebElement address;
	
	@FindBy (xpath = "//input[@id='address2']")
	private WebElement address2;
	
	@FindBy (xpath = "//input[@id='city']" )
	private WebElement city;
	
	@FindBy (xpath = "//select[@id='id_country']" )
	private WebElement country;
	
	@FindBy (xpath = "/html//input[@id='postcode']")
	private WebElement postcode;
	
	//@FindBy (xpath = "/html//input[@id='phone']" )
	//private WebElement phone;
	
	@FindBy (xpath = "/html//input[@id='phone_mobile']" )
	private WebElement mobile;
	
	@FindBy (xpath = "/html//input[@id='alias']" )
	private WebElement alias;
	
	@FindBy (xpath = "//button[@id='submitAccount']")
	private WebElement submitBTN;
	

	public void Registrationform (DtoUser user) {
		
		CheckBoxPage checkboxGender = new CheckBoxPage (driver);
		if (user.getGenero().equalsIgnoreCase("Male")) {
			checkboxGender.CheckradioBtn1(true);
		}
		
		if (user.getGenero().equalsIgnoreCase("Female")) {
			checkboxGender.checkradioBtn2(true);
		}
		
		firstname.sendKeys(user.getNombre());
		lastname.sendKeys(user.getApellido());
		password.sendKeys(user.getContrasena());
		
		DropDownPage dropdownDays = new DropDownPage(driver);
		dropdownDays.selectOptionByValueDays(user.getDianac());
		
		DropDownPage dropdownMonths = new DropDownPage(driver);
		dropdownMonths.selectOptionByValueMonths(user.getMesnac());
		
		DropDownPage dropdownYears = new DropDownPage(driver);
		dropdownYears.selectOptionByValueYears(user.getAnonac());
		
		company.sendKeys(user.getEmpresa());
		address.sendKeys(user.getDireccion());
		city.sendKeys(user.getCiudad());
		
		DropDownPage dropdownState = new DropDownPage(driver);
		dropdownState.selectOptionByValueState(user.getEstado());
		
		DropDownPage dropdownCountry = new DropDownPage (driver);
		dropdownCountry.selectOptionByValueCountry(user.getPais());
		
		postcode.sendKeys(user.getCodpostal());
		//phone.sendKeys(user.getPhone());
		mobile.sendKeys(user.getMobile());
		alias.clear();
		alias.sendKeys(user.getAlias());
		
	}
	
	public AccountPage clickRegister() {
		submitBTN.click();
		return new AccountPage(driver); }

	
}
