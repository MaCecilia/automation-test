package com.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CheckBoxPage extends BasePage {

	public CheckBoxPage(WebDriver driver) {
		super(driver);
		}
	
		@FindBy (xpath = "//input[@id='id_gender1']" )
		private WebElement radioBtn1;
		
		@FindBy (xpath = "//input[@id='id_gender2']")
		private WebElement radioBtn2;
		
		public void CheckradioBtn1 (boolean isChecked) {
			if (isChecked) {
				radioBtn1.isSelected();
				radioBtn1.click();
			}
		}
		
		public void checkradioBtn2 (boolean isChecked) {
			if(isChecked) {
				radioBtn2.isSelected();
				radioBtn2.click();
			}
		}
		
}
